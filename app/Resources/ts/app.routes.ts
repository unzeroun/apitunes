import {Routes} from "@angular/router";

import {HomeComponent} from './component/home';
import {QuoteTopComponent} from './component/quoteTop';
import {QuoteFormComponent} from './component/quoteForm';
import {QuoteComponent} from './component/quote';

export const rootRouterConfig: Routes = [
    {path: '', redirectTo: 'quotes', pathMatch: 'full'},
    {path: 'quotes', component: HomeComponent},
    {path: 'quotes/page/:page', component: HomeComponent},
    {path: 'quotes/top', component: QuoteTopComponent},
    {path: 'quotes/new', component: QuoteFormComponent},
    {path: 'quotes/:id', component: QuoteComponent},
];