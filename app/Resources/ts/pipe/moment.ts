import * as moment from 'moment';
import {Pipe, PipeTransform} from '@angular/core'

@Pipe({name: 'moment'})
export class MomentPipe implements PipeTransform
{
    transform(value: Date, format: string) {
        if ('fromNow' === format) {

            return moment(value).fromNow();
        }

        return moment(value).format(format);
    }
}
