import { Injectable } from '@angular/core';

@Injectable()
export class Configuration
{
    values = {};

    getValues() {
        return this.values;
    }

    setValues(values: Object) {
        this.values = values;
    }

    get(key: string, defaultValue: any = null) {
        return this.has(key) ? this.values[key] : defaultValue;
    }

    set(key: string, value: any) {
        this.values[key] = value;
    }

    has(key: string) {
        return undefined !== this.values[key];
    }
}
