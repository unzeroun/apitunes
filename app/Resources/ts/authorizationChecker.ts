import {Configuration} from './configuration';
import {Injectable} from '@angular/core';

@Injectable()
export class AuthorizationChecker
{
    constructor(public config: Configuration) { }

    get isAuthenticated(): boolean {
        return !!this.config.get('api.access_token');
    }

    isGranted(role) {
        return -1 !== this.config.get('security.roles').indexOf(role);
    }
}
