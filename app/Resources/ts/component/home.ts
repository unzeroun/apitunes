import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {QuoteRepository} from "../model/repository/quote";
import {QuoteCollection} from "../model/quoteCollection";

interface PagerLink {
    active: boolean,
    page:   number,
    text:   string
}

@Component({
    selector: 'home',
    templateUrl: './templates/home.html'
})
export class HomeComponent implements OnInit {
    public quotes: QuoteCollection = new QuoteCollection([], 0);
    public links: PagerLink[] = [];

    constructor(private quoteRepository: QuoteRepository, private route: ActivatedRoute) {

    }

    ngOnInit(): void {
        this.route.params.subscribe(params => {
            let currentPage = parseInt(params['page']) || 1;

            this.quoteRepository
                .findLasts(currentPage)
                .subscribe(
                    data => {
                        this.quotes = data;

                        this.links = [];
                        for (let i = 0; i < this.quotes.totalItems / 10; i++) {
                            this.links.push({
                                active: (currentPage) === i + 1,
                                page:   i + 1,
                                text:   (i + 1).toString()
                            });
                        }
                    },
                    err => console.log(err)
                );
        });
    }
}
