import {Component, OnInit} from '@angular/core';
import {QuoteRepository} from "../model/repository/quote";
import {QuoteCollection} from "../model/quoteCollection";
import {ActivatedRoute} from "@angular/router";

@Component({
    selector: 'quote-top',
    templateUrl: 'templates/quote-top.html',
})
export class QuoteTopComponent implements OnInit
{
    quotes = new QuoteCollection([], 0);
    links = [];

    constructor(private quoteRepository: QuoteRepository, private route: ActivatedRoute) {
    }

    ngOnInit(): void {
        this.route.params.subscribe(params => {
            let currentPage = parseInt(params['page']) || 1;

            this.quoteRepository
                .findBestRateds(currentPage)
                .subscribe(
                    data => {
                        this.quotes = data;

                        for (let i = 0; i < this.quotes.totalItems / 10 ;i++) {
                            this.links.push({active: (currentPage) === i + 1, page: i+1, text: i+1});
                        }
                    },
                    err  => console.log(err)
                );
        });
    }
}
