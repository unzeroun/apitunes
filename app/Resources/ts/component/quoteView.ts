import {Component, Attribute, Input} from '@angular/core';
import {Quote} from "../model/quote";
import {QuoteRepository} from "../model/repository/quote";

@Component({
    selector: 'quote-view',
    templateUrl: 'templates/quote-view.html',
})
export class QuoteViewComponent
{
    private _quote: Quote;

    @Input()
    set quote(quote: Quote) {
        this._quote = quote;
    }

    get quote(): Quote { return this._quote; }

    constructor(public quoteRepository: QuoteRepository) {}

    voteUp() {
        this.quoteRepository
            .voteUp(this._quote)
            .subscribe(quote => this._quote = quote);
    }

    voteDown() {
        this.quoteRepository
            .voteDown(this._quote)
            .subscribe(quote => this._quote = quote);
    }
}
