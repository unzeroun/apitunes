import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Quote} from "../model/quote";
import {QuoteRepository} from "../model/repository/quote";

@Component({selector: 'quote', templateUrl: './templates/quote.html'})
export class QuoteComponent implements OnInit {
    quote: Quote = new Quote({});

    constructor(private quoteRepository: QuoteRepository, private route: ActivatedRoute) {
    }

    ngOnInit(): void {
        this.route.params.subscribe(params => {
            this.quoteRepository
                .findOneById(params['id'])
                .subscribe(quote => this.quote = quote);
        });
    }
}
