import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {Quote} from "../model/quote";
import {QuoteRepository} from "../model/repository/quote";

@Component({
    selector: 'quote-form',
    templateUrl: './templates/quote-form.html'
})
export class QuoteFormComponent
{
    quote: Quote = new Quote({});

    constructor(private quoteRepository: QuoteRepository, private router: Router) {}

    onSubmit(hf) {
        if (!hf.form.valid) {
            return;
        }

        this.quoteRepository
            .persist(this.quote)
            .subscribe(quote => this.router.navigate(['/quotes/' + quote.id]));
    }
}
