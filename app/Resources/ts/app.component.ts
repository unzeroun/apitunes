import {Component, ElementRef} from '@angular/core';
import {RequestOptions} from '@angular/http';
import {Configuration} from './configuration';
import {AuthorizationChecker} from "./authorizationChecker";

@Component({
    selector: 'apitunes',
    templateUrl: './component/templates/layout.html',
})
export class AppComponent
{
    constructor(
        public elementRef: ElementRef,
        public config: Configuration,
        public authorizationChecker: AuthorizationChecker,
        requestOptions: RequestOptions
    ) {
        var element = elementRef.nativeElement;
        config.set('api.endpoint',      element.getAttribute('[api-endpoint]'));
        config.set('api.access_token',  element.getAttribute('[api-access-token]'));
        config.set('oauth.connect_url', element.getAttribute('[oauth-connect-url]'));
        config.set('auth.logout_url',   element.getAttribute('[auth-logout-url]'));
        config.set('security.roles',    JSON.parse(element.getAttribute('[security-roles]')));

        if(!!config.get('api.access_token')) {
            requestOptions.headers.append('Authorization', 'Bearer ' + config.get('api.access_token'))
        }
    }
}
