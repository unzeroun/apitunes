import {NgModule} from '@angular/core';
import {HttpModule} from "@angular/http";
import {FormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";
import {BrowserModule} from "@angular/platform-browser"

import {rootRouterConfig} from "./app.routes";
import {AppComponent} from "./app.component";
import {HomeComponent} from "./component/home";
import {QuoteComponent} from "./component/quote";
import {QuoteFormComponent} from "./component/quoteForm";
import {QuoteTopComponent} from "./component/quoteTop";
import {QuoteViewComponent} from "./component/quoteView";
import {MomentPipe} from "./pipe/moment";
import {QuotifyPipe} from "./pipe/quotify";
import {QuoteRepository} from "./model/repository/quote"
import {Configuration} from "./configuration";
import {AuthorizationChecker} from "./authorizationChecker"

@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        QuoteComponent,
        QuoteFormComponent,
        QuoteTopComponent,
        QuoteViewComponent,
        MomentPipe,
        QuotifyPipe,
    ],
    imports: [
        HttpModule,
        FormsModule,
        BrowserModule,
        RouterModule.forRoot(rootRouterConfig)
    ],
    providers: [QuoteRepository, Configuration, AuthorizationChecker],
    bootstrap: [AppComponent]
})
export class AppModule {

}