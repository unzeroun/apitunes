<?php

namespace AppBundle\Api;

/**
 * @author Yohan Giarelli <yohan@giarel.li>
 */
interface SlackApi
{
    /**
     * @param string $message
     *
     * @return \Guzzle\Http\Message\EntityEnclosingRequestInterface
     */
    public function speakFromSlackbot($message);
}
