<?php

namespace AppBundle\Api;

/**
 * @author Yohan Giarelli <yohan@giarel.li>
 */
class NullSlackApi implements SlackApi
{
    /**
     * @param string $message
     *
     * @return \Guzzle\Http\Message\EntityEnclosingRequestInterface
     */
    public function speakFromSlackbot($message)
    {
        return;
    }
}
