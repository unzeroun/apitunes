<?php

namespace AppBundle\Api;

use Guzzle\Http\ClientInterface;

/**
 * @author Yohan Giarelli <yohan@giarel.li>
 */
class GuzzleSlackApi implements SlackApi
{
    /**
     * @var ClientInterface
     */
    private $client;

    /**
     * @var array
     */
    private $config;

    public function __construct(ClientInterface $client, array $config)
    {
        $this->client = $client;
        $this->config = $config;
    }

    /**
     * @param string $message
     *
     * @return \Guzzle\Http\Message\EntityEnclosingRequestInterface
     */
    public function speakFromSlackbot($message)
    {
        $request = $this->client->post(
            $this->config['hook_url'] . '?' . http_build_query([
                'token'   => $this->config['token'],
                'channel' => $this->config['channel']
            ]),
            [],
            $message
        );

        return $this->client->send($request);
    }
}
