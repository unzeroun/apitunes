<?php

namespace AppBundle\Command;

use AppBundle\Entity\Quote;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Yaml\Yaml;

/**
 * @author Yohan Giarelli <yohan@giarel.li>
 */
class ImportQuoteCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('apitunes:quote:import');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $data = Yaml::parse($this->getContainer()->getParameter('kernel.root_dir') . '/data/fortune.yml');

        foreach ($data as $quoteData) {
            $this->importOne($io, $quoteData);
        }

        $this->getContainer()->get('doctrine')->getManager()->flush();
    }

    private function importOne(SymfonyStyle $io, $data)
    {
        $quote = new Quote();
        $quote->setAuthor($data['author']);
        $quote->setTitle($data['title']);
        $quote->setText($data['content']);
        $quote->setVotes($data['votes']);
        $quote->setPublishedAt(\DateTime::createFromFormat('U', $data['created_at']));

        $this->getContainer()->get('doctrine')->getManager()->persist($quote);
    }
}
