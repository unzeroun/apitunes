<?php

namespace AppBundle\DependencyInjection;

use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * @author Yohan Giarelli <yohan@giarel.li>
 */
class AppExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = $this->getConfiguration($configs, $container);
        $config = $this->processConfiguration($configuration, $configs);

        $container->setParameter('app.slack', $config['slack']);

        if (null !== $config['slack']['token']) {
            $container->setAlias('api.slack', 'api.slack.guzzle');
        } else {
            $container->setAlias('api.slack', 'api.slack.null');
        }
    }
}
