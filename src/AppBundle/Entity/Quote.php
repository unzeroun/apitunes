<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Dunglas\ApiBundle\Annotation as Api;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="fortune")
 * @ORM\HasLifecycleCallbacks
 *
 * @author Yohan Giarelli <yohan@giarel.li>
 */
class Quote
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     * @Api\Iri("https://schema.org/title")
     * @Assert\NotBlank
     *
     * @var string
     */
    private $title;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank
     *
     * @var string
     */
    private $author;

    /**
     * @ORM\Column(type="text", name="content")
     * @Assert\NotBlank
     *
     * @var string
     */
    private $text;

    /**
     * @ORM\Column(type="integer", name="votes")
     * @Assert\NotBlank
     *
     * @var \DateTime
     */
    private $vote;

    /**
     * @ORM\Column(type="datetime", name="created_at")
     * @Assert\NotBlank
     *
     * @var \DateTime
     */
    private $publishedAt;

    /**
     * @ORM\Column(type="datetime", name="updated_at")
     * @Assert\NotBlank
     *
     * @var \DateTime
     */
    private $updatedAt;

    public function __construct()
    {
        $this->publishedAt = new \DateTime;
        $this->updatedAt   = new \DateTime;
        $this->vote        = 0;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param string $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @param $vote
     */
    public function setVotes($vote)
    {
        $this->vote = $vote;
    }

    public function voteUp()
    {
        $this->vote++;
    }

    public function voteDown()
    {
        $this->vote--;
    }

    /**
     * @return \DateTime
     */
    public function getVote()
    {
        return $this->vote;
    }

    /**
     * @return \DateTime
     */
    public function getPublishedAt()
    {
        return $this->publishedAt;
    }

    /**
     * @param \DateTime $publishedAt
     */
    public function setPublishedAt(\DateTime $publishedAt)
    {
        $this->publishedAt = $publishedAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @ORM\PreUpdate
     */
    public function onUpdate()
    {
        $this->updatedAt = new \DateTime;
    }
}
