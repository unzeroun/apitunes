<?php

namespace AppBundle\ValueObject;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Embeddable
 *
 * @author Yohan Giarelli <yohan@giarel.li>
 */
class AccessToken
{
    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTimeInterface
     */
    private $expires;

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     * @var string
     */
    private $accessToken;

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     * @var string
     */
    private $refreshToken;

    public function __construct(\DateTimeInterface $dateTime, $accessToken, $refreshToken = null)
    {
        $this->expires      = $dateTime;
        $this->accessToken  = $accessToken;
        $this->refreshToken = $refreshToken;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getExpires()
    {
        return $this->expires;
    }

    /**
     * @return string
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }

    /**
     * @return string
     */
    public function getRefreshToken()
    {
        return $this->refreshToken;
    }
}
