<?php

namespace AppBundle\Subscriber;

use Beberlei\Metrics\Collector\Collector;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Stopwatch\Stopwatch;

/**
 * @author Yohan Giarelli <yohan@giarel.li>
 */
class MetricKernelSubscriber implements EventSubscriberInterface
{
    /**
     * @var Collector
     */
    private $collector;

    /**
     * @var Stopwatch
     */
    private $stopwatch;

    public function __construct(Collector $collector, Stopwatch $stopwatch = null)
    {
        $this->collector = $collector;
        $this->stopwatch = $stopwatch ?: new Stopwatch;
    }

    public function onKernelRequest()
    {
        $this->stopwatch->start('app.time');
    }

    public function onKernelResponse()
    {
        try {
            $event = $this->stopwatch->stop('app.time');
            $this->collector->timing('app.time', $event->getDuration());
        } catch (\Exception $e) {}
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST  => ['onKernelRequest',   1024],
            KernelEvents::RESPONSE => ['onKernelResponse', -1024],
        ];
    }
}
