<?php

namespace AppBundle\Subscriber;

use AppBundle\Entity\Quote;
use Dunglas\ApiBundle\Event\DataEvent;
use Dunglas\ApiBundle\Event\Events;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

/**
 * @author Yohan Giarelli <yohan@giarel.li>
 */
class QuoteAuthorizationSubscriber implements EventSubscriberInterface
{
    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    public function __construct(AuthorizationCheckerInterface $authorizationChecker, TokenStorageInterface $tokenStorage)
    {
        $this->authorizationChecker = $authorizationChecker;
        $this->tokenStorage         = $tokenStorage;
    }

    public function onPreCreate(DataEvent $event)
    {
        if (!$event->getData() instanceof Quote) {
            return;
        }

        if ($this->tokenStorage->getToken() instanceof AnonymousToken) {
            throw new AuthenticationException('You are not authenticated', 401);
        }

        if (!$this->authorizationChecker->isGranted('ROLE_CREATE_QUOTE')) {
            throw new AccessDeniedException('You are not allowed to create a quote');
        }
    }

    public function onPreUpdate(DataEvent $event)
    {
        if (!$event->getData() instanceof Quote) {
            return;
        }

        if (!$this->authorizationChecker->isGranted('ROLE_UPDATE_QUOTE')) {
            throw new AccessDeniedException('You are not allowed to create a quote');
        }
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            Events::PRE_CREATE_VALIDATION => 'onPreCreate',
            Events::PRE_UPDATE_VALIDATION => 'onPreUpdate',
        ];
    }
}
