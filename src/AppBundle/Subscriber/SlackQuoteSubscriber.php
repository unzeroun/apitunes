<?php

namespace AppBundle\Subscriber;

use AppBundle\Api\SlackApi;
use AppBundle\Entity\Quote;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * @author Yohan Giarelli <yohan@giarel.li>
 */
class SlackQuoteSubscriber implements EventSubscriber
{
    /**
     * @var SlackApi
     */
    private $slackApi;

    /**
     * @var UrlGeneratorInterface
     */
    private $urlGenerator;

    public function __construct(SlackApi $api, UrlGeneratorInterface $urlGenerator)
    {
        $this->slackApi     = $api;
        $this->urlGenerator = $urlGenerator;
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postPersist(LifecycleEventArgs $args)
    {
        $quote = $args->getEntity();
        if (!$quote instanceof Quote) {
            return;
        }

        $url = $this->urlGenerator->generate(
            'quotes_show', ['id' => $quote->getId()],
            UrlGeneratorInterface::ABSOLUTE_URL
        );

        $this->slackApi->speakFromSlackbot(
            <<<MD
Une fortune a été postée par {$quote->getAuthor()}:
```
{$quote->getText()}
```
{$url}
MD
        );
    }

    /**
     * @return array
     */
    public function getSubscribedEvents()
    {
        return [
            Events::postPersist,
        ];
    }
}
