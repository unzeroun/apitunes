<?php

namespace AppBundle\Security;

use AppBundle\Entity\Repository\UserRepository;
use AppBundle\Entity\User;
use AppBundle\ValueObject\AccessToken;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\NoResultException;
use HWI\Bundle\OAuthBundle\OAuth\ResourceOwner\GoogleResourceOwner;
use HWI\Bundle\OAuthBundle\OAuth\ResourceOwnerInterface;
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\Security\Core\User\OAuthAwareUserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * @author Yohan Giarelli <yohan@giarel.li>
 */
class UserProvider implements OAuthAwareUserProviderInterface, UserProviderInterface
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var ResourceOwnerInterface
     */
    private $resourceOwner;

    public function __construct(UserRepository $userRepository, GoogleResourceOwner $resourceOwner)
    {
        $this->userRepository = $userRepository;
        $this->resourceOwner  = $resourceOwner;
    }

    /**
     * @param UserResponseInterface $response
     *
     * @return User
     */
    public function loadUserByOAuthUserResponse(UserResponseInterface $response)
    {
        try {
            $user = $this->loadUserByUsername($response->getEmail());
        } catch (DBALException $e) {
            throw new \RuntimeException('Invalid credentials', 401);
        }

        if (!$response->getOAuthToken()->getExpiresAt()) {
            return $user;
        }

        $user->setAccessToken(new AccessToken(
            \DateTimeImmutable::createFromFormat('U', $response->getOAuthToken()->getExpiresAt()),
            $response->getAccessToken(),
            $response->getRefreshToken()
        ));

        $this->userRepository->update($user);

        return $user;
    }

    /**
     * @param string $username
     *
     * @return User
     */
    public function loadUserByUsername($username)
    {
        try {
            return $this->userRepository->findByUsername($username);
        } catch (NoResultException $e) {
            $user = new User($username);
            $this->userRepository->persist($user);

            return $user;
        }
    }

    /**
     * @param UserInterface $user
     *
     * @return User
     */
    public function refreshUser(UserInterface $user)
    {
        $user = $this->loadUserByUsername($user->getUsername());
        if ($user->getAccessToken()->getExpires() < new \DateTimeImmutable) {
            $response = $this->resourceOwner->refreshAccessToken($user->getAccessToken()->getRefreshToken());

            $user->setAccessToken(new AccessToken(
                new \DateTimeImmutable('+' . $response['expires_in'] . ' seconds'),
                $response['access_token'],
                $user->getAccessToken()->getRefreshToken()
            ));

            $this->userRepository->update($user);
        }

        return $user;
    }

    /**
     * @param string $class
     *
     * @return bool
     */
    public function supportsClass($class)
    {
        return $class === User::class;
    }
}
