<?php

namespace AppBundle\Security;

use HWI\Bundle\OAuthBundle\DependencyInjection\Security\Factory\OAuthFactory;
use Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\AbstractFactory;

class AccessTokenFactory extends OAuthFactory
{
    protected function createEntryPoint($container, $id, $config, $defaultEntryPoint)
    {
        return AbstractFactory::createEntryPoint($container, $id, $config, $defaultEntryPoint);
    }

    public function getPosition()
    {
        return 'pre_auth';
    }

    protected function getListenerId()
    {
        return 'authentication.listener.access_token';
    }

    public function getKey()
    {
        return 'access_token';
    }
}
