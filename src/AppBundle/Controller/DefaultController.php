<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration as Extra;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Role\Role;

/**
 * @Extra\Route("/")
 *
 * @author Yohan Giarelli <yohan@giarel.li>
 */
class DefaultController extends Controller
{

    /**
     * @Extra\Route("/assets/{extension}/{name}", name="redirect_to_asset_bundle", host="%domain%")
     *
     * @param string $name
     * @param string $extension
     *
     * @return RedirectResponse
     */
    public function redirectToAssetBundleAction($name, $extension)
    {
        $finder = Finder::create()
            ->files()
            ->in($this->getParameter('kernel.root_dir') . '/../web/assets')
            ->name("${name}.*.bundle.${extension}")
            ->name("${name}.bundle.${extension}")
            ->sortByChangedTime()
            ->getIterator();

        return new RedirectResponse('/assets/' . end($finder)->getFilename());
    }

    /**
     * @Extra\Route("/", name="homepage", host="%domain%")
     * @Extra\Route("/quotes/{id}", name="quotes_show", host="%domain%")
     * @Extra\Route("{path}", name="catch_all", requirements={"path": ".*"}, host="%domain%")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $roles          = $this->getUser() ? $this->getUser()->getRoles() : [];
        $reachableRoles = $this->get('security.role_hierarchy')->getReachableRoles(
            array_map(
                function ($role) { return new Role($role); },
                $roles
            )
        );

        return $this->render(
            'default/index.html.twig',
            [
                'reachable_roles' => array_map(function(Role $role) { return $role->getRole(); }, $reachableRoles)
            ]
        );
    }
}
