#!/usr/bin/env bash

composer self-update

if [ "${SYMFONY_ENV}" = "prod" ]
then
    composer install --no-dev
else
    composer install

    export WEB_CONCURRENCY=1
    /www/vendor/bin/heroku-php-nginx -C nginx_app.conf -F docker/php-fpm.conf web/
fi
