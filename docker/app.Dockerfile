FROM docker.un-zero-un.net/php:7.1-node

RUN apt-get update \
 && apt-cache search php7.1 \
 && apt-get install -y --no-install-recommends nginx libpq-dev \
 && rm -rf /var/lib/apt/lists/*

RUN docker-php-ext-install pdo_pgsql \
 && docker-php-ext-enable pdo_pgsql

COPY . /www
COPY docker/heroku.conf /etc/nginx/nginx.conf
COPY docker/entrypoint.sh /entrypoint.sh
WORKDIR /www

CMD ["/entrypoint.sh"]
